<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('main_info', function (Blueprint $table) {
            $table->id('main_id');
            $table->string('thumnail',255);
            $table->string('Hotel_name');
            $table->string('rate');
            $table->string('location');
            $table->string('language_spoken');
            $table->string('room');
            $table->string('bedroom');
            $table->string('bathroom');
            $table->string('descriptoin');
            $table->string('phone');
            $table->string('checkin');
            $table->string('checkout');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('main_info');
    }
};
