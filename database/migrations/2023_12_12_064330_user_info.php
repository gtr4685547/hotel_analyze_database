<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('user_info', function (Blueprint $table) {
            $table->id('user_info_id');
            $table->string('full_name',255);
            $table->string('id_card_no',255);
            $table->string('gender',255);
            $table->string('email',255);
            $table->string('dob',255);
            $table->string('address',255);
            $table->string('number',255);
            $table->string('Message',255);
            $table->string('condition',255);
            $table->string('request_thing',255);
            $table->string('nationality',255);
            $table->foreign('user_info_id')->references('user_id')->on('user_booking');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('room_type');
    }
};
