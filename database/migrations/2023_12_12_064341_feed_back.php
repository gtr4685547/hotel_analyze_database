<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('feed_back', function (Blueprint $table) {
            $table->id('feed_id');
            $table->string('full_name',255);
            $table->string('feed_back',255);
            $table->foreign('feed_id')->references('user_info_id')->on('user_info');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('feed_back');
    }
};
