<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('user_booking', function (Blueprint $table) {
            $table->id('user_id');
            $table->string('checkin',255);
            $table->string('checkout');
            $table->string('qty_room');
            $table->integer('adult');
            $table->string('children');
            $table->string('room_number');
            $table->integer('total_day');
            $table->foreign('user_id')->references('payment_id')->on('payment');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('user_booking');
    }
};
