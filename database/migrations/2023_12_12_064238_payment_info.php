<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('payment', function (Blueprint $table) {
            $table->id('payment_id');
            $table->string('name_card',255);
            $table->string('card_number',255);
            $table->string('expird_date',255);
            $table->string('price',255);
            $table->string('service',255);
            $table->string('service_charge',255);
            $table->string('tax',255);
            $table->string('vat',255);
            $table->string('total',255);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('payment');
    }
};
