<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('place_around', function (Blueprint $table) {
            $table->id('place_id');
            $table->string('parking',255);
            $table->string('gym');
            $table->string('swiming_pool');
            $table->string('Play grownd');
            $table->string('restaurant');
            $table->foreign('place_id')->references('sub_id')->on('sub_info');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('place_around');
    }
};
