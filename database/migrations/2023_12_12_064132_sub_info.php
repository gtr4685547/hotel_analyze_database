<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('sub_info', function (Blueprint $table) {
            $table->id('sub_id');
            $table->string('building',255);
            $table->string('policy');
            $table->string('Available_in_rooms');
            $table->string('Nearby');
            $table->string('Food');
            $table->string('Cleaning_And_safety');
            $table->string('relax_thing');
            $table->string('Internet_access');
            $table->string('adult');
            $table->string('property');
            $table->string('Hotel_Highlights');
            $table->foreign('sub_id')->references('main_id')->on('main_info');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('sub_info');
    }
};
